package org.erasmus.struts;


import java.util.HashMap;
import java.util.Map;

import org.erasmus.model.*;

public class CreateAction extends Action {
	/* Declarations */
	private Map langu;
	private Map formFields;
	
	public CreateAction() {
		langu = new HashMap<String, String>();
		langu.put("de", "Deutsch");
		langu.put("en", "Englisch");
		langu.put("pl", "Polnisch");	
		
	}

	/* Getter and Setter */
	public Map getLangu() {
		return langu;
	}

	public void setLangu(Map langu) {
		this.langu = langu;
	}

	public Map getFormFields() {
		return formFields;
	}

	public void setFormFields(Map formFields) {
		this.formFields = formFields;
	}
	
	
	
	
	
	
	
}
