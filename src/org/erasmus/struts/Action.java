package org.erasmus.struts;

import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;

public class Action implements SessionAware {

	Map<String, Object> session;

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	public String execute() throws Exception {
		return "success";
	}

}
