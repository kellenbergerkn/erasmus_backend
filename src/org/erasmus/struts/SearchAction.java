package org.erasmus.struts;


import java.util.HashMap;
import java.util.Map;

import org.erasmus.model.*;

public class SearchAction extends Action {
	/* Declarations */
	private String searchText;
	private String searchLangu;
	private Map langu;
	private Entry word;
	private char Mode;
	
	public SearchAction() {
		langu = new HashMap<String, String>();
		langu.put("de", "Deutsch");
		langu.put("en", "Englisch");
		langu.put("pl", "Polnisch");	
		
	}
	
	public String execute() throws Exception {
		if (getMode() == ' '|| getMode() == 0) {
			setMode('S');
		}
		if(getMode() == 'S') {
			search( );
		}
		return "success";
	}
	private void search() {
		
		FindTranslations searchModel = new FindTranslations();
		try {
			word = searchModel.search(getSearchLangu(), getSearchText() );
		} catch (Exception e) {

		}
		if(word == null) {
			Language tmpLangu = new Language();
			tmpLangu.setLanguage("de");
			Word tmpWord = new Word();
			tmpWord.setWord("Apfel");
			Map<Language, Word> tmpMap = new HashMap();
			tmpMap.put(tmpLangu, tmpWord);
			word = new Entry();
			word.setLanguageTranslationMap(tmpMap);
		}
		
	}
	
	
	/* Getter and Setter */
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getSearchLangu() {
		return searchLangu;
	}
	public void setSearchLangu(String searchLangu) {
		this.searchLangu = searchLangu;
	}
	public Map getLangu() {
		return langu;
	}
	public void setLangu(Map langu) {
		this.langu = langu;
	}

	public Entry getWord() {
		return word;
	}
	
	public void setWord(Entry word) {
		this.word = word;
	}

	public char getMode() {
		return Mode;
	}

	public void setMode(char mode) {
		Mode = mode;
	}
	
	
	
	
	
}
