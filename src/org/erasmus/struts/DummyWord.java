package org.erasmus.struts;

import java.util.HashMap;
import java.util.Map;

public class DummyWord {
	
	private Map translation;
	private String url;
	
	public DummyWord() {
		translation = new HashMap<String, String>();
		translation.put("de", "Affe");
		translation.put("en", "Ape");
		translation.put("pl", "SGBJDhdccd");
		
		this.url = "test.jpg";
	}

	public Map getTranslation() {
		return translation;
	}

	public void setTranslation(Map translation) {
		this.translation = translation;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
	

}
