package org.erasmus.model;

public class Word {

	private long id;
	private String word;
	private String pathToAudioFile;
	private long languageId;
	private long entryId;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) { //TODO: database sequence to generate id? generation necessary?
		this.id = id;
	}
	
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getDtWord() {
		return word;
	}
	public void setDtWord(String dtWord) {
		this.word = dtWord;
	}
	public String getDtAudioDirectory() {
		return pathToAudioFile;
	}
	public void setDtAudioDirectory(String dtAudioDirectory) {
		this.pathToAudioFile = dtAudioDirectory;
	}
	public long getFiLanguage() {
		return languageId;
	}
	public void setFiLanguage(long fiLanguage) {
		this.languageId = fiLanguage;
	}
	public long getFiEntry() {
		return entryId;
	}
	public void setFiEntry(long fiEntry) {
		this.entryId = fiEntry;
	}
	
	
}
