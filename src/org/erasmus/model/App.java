package org.erasmus.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class App {

	public static void main(String[] args) throws Exception {
//		doDatabaseStuff();
		new FindTranslations().search("German", "Apfel");
	}

	private static void doDatabaseStuff() throws Exception {
		DataSource dataSource = new DataSource();

		try {
			String databaseProperties = "./Erasmus-Project/src/erasmus/database.properties";

			dataSource.openConnection(databaseProperties);
			System.out.println("Connection opened!");

			String query = "SELECT * FROM tdtEntries;";
			ResultSet resultSet = dataSource.selectData(query);		
			
			List<String> columnNames = new ArrayList<>();
			
			columnNames.add("idEntry");
			columnNames.add("fiImage");
			
			printResultSet(resultSet, columnNames);

	        
		} finally {
			dataSource.closeConnection();
			System.out.println("Connection closed!");
		}
	}
	
	private static void printResultSet(ResultSet resultSet, List<String> columnNames) throws SQLException
	{
		while(resultSet.next())
		{
			for(int j = 0; j < columnNames.size(); j++)
			{
				String columnName = columnNames.get(j);
				
				System.out.print("[" + columnName + "=" + resultSet.getInt(columnName) + "] ");
			}
			
			System.out.println();
		}
	}
}
