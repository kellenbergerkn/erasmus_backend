package org.erasmus.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FindTranslations {
	public Entry search(String language, String word) throws Exception {
		Entry result= new Entry();
		DataSource dataSource = new DataSource();

		try {
			String databaseProperties = "./Erasmus-Project/src/erasmus/database.properties";
	
			dataSource.openConnection(databaseProperties);
			System.out.println("Connection opened!");
	
//			String query = "SELECT tl.dtName, tw.dtWord "
//					+ "FROM tdtWords as tw"
//					+ "JOIN tdtLanguages as tl"
//					+ "ON tw.fiLanguage = tl.idLanguage"
//					+ "WHERE tw.dtWord = " + "'Apfel'"
//					+ "AND tl.dtName = " + "'German'";
			
			String query =
					"SELECT tl.dtName, tw.dtWord " + 
					"FROM tdtWords as tw " +
					"JOIN tdtLanguages as tl ON tw.fiLanguage = tl.idLanguage " + 
					"WHERE tw.dtWord = 'Apfel' " + 
					"AND tl.dtName = 'German';";
			ResultSet resultSet = dataSource.selectData(query);
			
			List<String> columnNames = new ArrayList<>();
			
			columnNames.add("dtName");
			columnNames.add("dtWord");
			
			printResultSet(resultSet, columnNames);
			
		} finally {
			dataSource.closeConnection();
			System.out.println("Connection closed!");
		}
		
		return result;
	}
	
	private void printResultSet(ResultSet resultSet, List<String> columnNames) throws SQLException
	{
		while(resultSet.next())
		{
			for(int j = 0; j < columnNames.size(); j++)
			{
				String columnName = columnNames.get(j);
				
				try {
					System.out.print("[" + columnName + "=" + resultSet.getInt(columnName) + "] ");
				}
				catch (NumberFormatException e)
				{
					System.out.print("[" + columnName + "=" + resultSet.getString(columnName) + "] ");
				}
				
				
			}
			
			System.out.println();
		}
	}
}
