package org.erasmus.model;

import java.util.Map;

public class Entry {
	Map<Language, Word> languageTranslationMap;
	String pathToPicture;

	public Entry() {
		languageTranslationMap = null;
		pathToPicture = "";
	}

	public void changePicturePath(String newPicturePath) { // TODO: refactor
		System.out.println("Old path to picture: " + pathToPicture);
		pathToPicture = newPicturePath;
		System.out.println("replaced with: " + newPicturePath);
	}

	public void changeTranslation(String language, String translation) { // TODO: refactor?
//		String oldTranslation = languageTranslationMap.put(language, translation);
//		System.out.println("Old translation value: " + oldTranslation);
		System.out.println("replaced with: " + translation);
	}

	/* Getter und Setter */
	public Map<Language, Word> getLanguageTranslationMap() {
		return languageTranslationMap;
	}

	public void setLanguageTranslationMap(Map<Language, Word> languageTranslationMap) {
		this.languageTranslationMap = languageTranslationMap;
	}

	public String getPathToPicture() {
		return pathToPicture;
	}

	public void setPathToPicture(String pathToPicture) {
		this.pathToPicture = pathToPicture;
	}

}
