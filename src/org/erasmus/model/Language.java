package org.erasmus.model;

public class Language {

	private long id;
	private String language;
	public long getId() {
		return id;
	}
	
	public void setId(long id) {//TODO: database sequence to generate id? generation necessary?
		this.id = id;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	
}
