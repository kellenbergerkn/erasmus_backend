package org.erasmus.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DataSource {

	private Connection connection;

	public Properties getDatabaseProperties(String filePath) throws FileNotFoundException, IOException {
		PropertiesProvider propertiesProvider = new PropertiesProvider();
		return propertiesProvider.getProperties(filePath);
	}

	public ResultSet selectData(String query) throws SQLException {
		PreparedStatement selectStatment = connection.prepareStatement(query);
		System.out.println("selectStatment.execute() - " + selectStatment.execute());
		return selectStatment.getResultSet();
	}

	private String getConnectionString(String host, String port, String databaseName) {
		return String.format("jdbc:mysql://%s:%s/%s", host, port, databaseName);
	}

	public void openConnection(String databasePropertiesFilePath)
			throws SQLException, ReflectiveOperationException, FileNotFoundException, IOException {
		Properties database = getDatabaseProperties(databasePropertiesFilePath);
		openConnection(database);
	}

	private void openConnection(Properties database) throws SQLException, ReflectiveOperationException {
		Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

		String connectionString = getConnectionString(database.getProperty("database.host"),
				database.getProperty("database.port"), database.getProperty("database.dbName"));

		System.out.println(connectionString);

		connection = DriverManager.getConnection(connectionString, database.getProperty("database.user"),
				database.getProperty("database.password"));
	}

	public void closeConnection() throws SQLException {
		if (connection != null && !connection.isClosed()) {
			connection.close();
		}
	}
}