<%@ page contentType="text/html; charset = UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
<head>
<link rel="stylesheet" href="../mimes/css/bootstrap.min.css">
<link rel="stylesheet" href="../mimes/css/stylesheet.css">
<meta charset="UTF-8">
</head>
<body>
	<div class="container">
		<!--Body-->
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="index.html"> <img
				src="../mimes/images/logo_erasmus_header.png" height="55px" alt="">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link"
						href="<s:url action='search'/>">Startseite <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item"><a class="nav-link"
						href="search">Übersicht</a></li>
				</ul>
				<form class="form-inline my-2 my-lg-0">
					<select class="dropdown-langu form-control form-control-lg">
						<option value="de">Deutsch</option>
						<option value="en">English</option>
					</select>
				</form>
			</div>
		</nav>
		<form action="search">
			<div class="languageSelect row">
				<div class="languageFrom col-md-12">
					<s:select list="langu" class="form-control form-control-lg" name="searchLangu"/>
				</div>
			</div>
			<div class="searchfield row">
				<!--Suche-->
				<s:textfield type="text" placeholder="Suche"
					class="form-control col-md-12" name="searchText" value="%{searchText}"/>
				<button class="btn btn-primary col-md-12">Suche</button>
				<a class="btn btn-primary col-md-12" href="<s:url action='create'/>"> Anlegen</a>
			</div>
		</form>
		<s:if test="%{word != null}">
		<div class="row result">
			<div class="image col-xs-12 col-md-4">
				<image class="row" src="images/hammer.jpg"></image>
			</div>
			<div class="translations col-xs-12 col-md-8">
			<div class="translations-header"> 
					<div class="row">
						<div class="col-xs-3">Sprache</div> 
						<div class="col-xs-3">Wort</div>
					</div>>
				</div>
			<s:iterator value="word.languageTranslationMap" var="translation">
			<div class="lang <s:property value="key.language"/>"> 
					<div class="row">
						<div class="col-xs-3"><s:property value="key.language"/></div> 
						<div class="col-xs-3"><s:property value="value.word"/></div>
					</div>
<!-- 					<div class="attachements row"> -->
<%-- 						<span class="col-xs-12">Attachments:</span> --%>
<!-- 					</div> -->
				</div>
			</s:iterator>
			</div>
		</div>
		</s:if>
		<s:else>
		<h1>Keine Wörter gefunden!</h1>
		</s:else>

	</div>
	<script defer
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script defer
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	<script defer
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono"
		rel="stylesheet">
	<link rel="stylesheet"
		href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</body>
</html>